﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fall : MonoBehaviour {

    public OVRInput.Controller leftHand;
    public OVRInput.Controller rightHand;

    public string rightThumbY;
    public string leftThumbY;

    private Rigidbody rb;
    private Quaternion initialRotation;

    private Vector3 direction = new Vector3(0f, 0f, 1f);
    private float speedForward = 100f;

    private Vector3 directionFall = new Vector3(0f, -1f, 0f);
    private float speedFall = 10f;

    private float MAX_UP_ANGLE = 10f;
    private float MAX_DOWN_ANGLE = 60f;
    private float DEGREE_DELTA = 0.005f;
    private float V_DEGREE_DELTA = 0.5f;

    private float orientationAngle = 0f;
    private float pitchAngle = 0f;

    AudioSource audioSource;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        initialRotation = rb.rotation;
    }

    // Update is called once per frame
    void Update() {
        // get both hands position
        Vector3 leftHandPos = OVRInput.GetLocalControllerPosition(leftHand);
        Vector3 rightHandPos = OVRInput.GetLocalControllerPosition(rightHand);

        bool keyboardDebug = false;
        if (leftHandPos == Vector3.zero && rightHandPos == Vector3.zero)
        {
            keyboardDebug = true;
        }

        // Resetting to initial rotation before applying the player's movement
        rb.transform.rotation = initialRotation; // Flat on belly

        // Debug with keyboard
        if (keyboardDebug)
        {
            //Turn
            if (Input.GetKey(KeyCode.RightArrow)) {
                leftHandPos = new Vector3(-7f, 2.5f, 0f);
                rightHandPos = new Vector3(7f, -2.5f, 0f);
            } else if (Input.GetKey(KeyCode.LeftArrow)) {
                leftHandPos = new Vector3(-7f, -2.5f, 0f);
                rightHandPos = new Vector3(7f, 2.5f, 0f);
            } else if (Input.GetKey(KeyCode.D)) {
                leftHandPos = new Vector3(-6.3f, 4f, 0f);
                rightHandPos = new Vector3(6.3f, -4f, 0f);
            } else if (Input.GetKey(KeyCode.A)) {
                leftHandPos = new Vector3(-6.3f, -4f, 0f);
                rightHandPos = new Vector3(6.3f, 4f, 0f);
            } else {
                leftHandPos = new Vector3(-7.5f, 0f, 0f);
                rightHandPos = new Vector3(7.5f, 0f, 0f);
            }
        }

        // calculate position difference between hands

        float diffX = leftHandPos.x - rightHandPos.x;
        float diffY = leftHandPos.y - rightHandPos.y;

        float armsLength = Mathf.Sqrt(Mathf.Pow(diffX, 2) + Mathf.Pow(diffY, 2));

        if (armsLength <= 1)
        {
            armsLength = 1;
        }

        // Applying side turn
        float turnAngle = 0f;

        //Negatif diffY = turn left
        //Positive diffY = turn right
        if (diffY != 0f) {
            turnAngle = 180 - Mathf.Abs(Mathf.Atan2(diffY, diffX) * 180f / Mathf.PI);
        }

        if (diffY > 0) {
            // To the right
            turnAngle = -turnAngle;
            orientationAngle = Mathf.Repeat((orientationAngle - Mathf.Abs(turnAngle) * DEGREE_DELTA) + 360, 360);
        } else if (diffY < 0) {
            // To the left
            orientationAngle = Mathf.Repeat((orientationAngle + Mathf.Abs(turnAngle) * DEGREE_DELTA) + 360, 360);
        }

        // Rotate character (Z)
        rb.transform.Rotate(new Vector3(0, 0, orientationAngle));

        //Pitch
        float thumbY = 0f;
        if (keyboardDebug)
        {
            //Keyboard debug
            if (Input.GetKey(KeyCode.UpArrow))
            {
                thumbY = -1f;
            }
            else if (Input.GetKey(KeyCode.W))
            {
                thumbY = -0.5f;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                thumbY = 1f;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                thumbY = 0.5f;
            }
        }
        else
        {
            thumbY = Input.GetAxis(rightThumbY);

            if (thumbY == 0)
            {
                thumbY = Input.GetAxis(leftThumbY);
            }
        }

        pitchAngle = pitchAngle + (V_DEGREE_DELTA * -thumbY);

        if (pitchAngle < -MAX_UP_ANGLE)
        {
            pitchAngle = -MAX_UP_ANGLE;
        } else if (pitchAngle > MAX_DOWN_ANGLE)
        {
            pitchAngle = MAX_DOWN_ANGLE;
        }

        // Debug.Log("pitchAngle: " + pitchAngle);
        float thumbAngleRadians = Mathf.Abs(pitchAngle) * Mathf.PI / 180;

        float yVector = Mathf.Sin(thumbAngleRadians) * 1;
        if(pitchAngle > 0)
        {
            yVector = -yVector;
        }
        float hLength = Mathf.Cos(thumbAngleRadians) * 1;

        // Rotate character (Y)
        rb.transform.Rotate(new Vector3(pitchAngle, 0, 0));

        // Rotate character (X)
        rb.transform.Rotate(new Vector3(0, turnAngle, 0));

        float reducedAngle = Mathf.Repeat(orientationAngle, 90);
        float orientationAngleRadians = Mathf.Repeat(orientationAngle, 90) * Mathf.PI / 180;

        float xVector = 0f;
        float zVector = 0f;

        if ((orientationAngle >= 0 && orientationAngle <= 90) || (orientationAngle >= 180 && orientationAngle <= 270))
        {
            xVector = Mathf.Abs(Mathf.Cos(orientationAngleRadians) * hLength);
            zVector = Mathf.Abs(Mathf.Sin(orientationAngleRadians) * hLength);
        }
        else
        {
            xVector = Mathf.Abs(Mathf.Sin(orientationAngleRadians) * hLength);
            zVector = Mathf.Abs(Mathf.Cos(orientationAngleRadians) * hLength);
        }
        
        if (orientationAngle > 180)
        {
            zVector = -zVector;
        }

        if (orientationAngle > 90 && orientationAngle < 270)
        {
            xVector = -xVector;
        }

        direction = new Vector3(xVector, yVector, zVector);

        float speedModif = 15 / armsLength;

        Vector3 force = direction * speedForward * speedModif + directionFall * speedFall * speedModif;
        rb.velocity = force;

        // Debug.Log("direction: (" + direction.x + ", " + direction.y + ", " + direction.z + ")");
        // Debug.Log("force: (" + force.x + ", " + force.y + ", " + force.z + ")");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            audioSource.Play();
            Destroy(other.gameObject);
        }
    }
}
